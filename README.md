# Hothouse KAM Stack

A boilerplate repo named after Hothouse developer Kam Hayes. It uses Angular 2+, Strapi, Mongo, and Docker. Feel free to FORK and enjoy!


## Initial Setup
1. Install dependencies: Node/npm, Angular CLI, Docker Community Edition
2. Make your Angular app by running "ng new app-name" (replace app-name with your app name)


## Returning Dev:
1. Go to the "app-name" folder
2. Run "ng serve" to get your Angular app running
3. Go to the "/strapi-docker" folder
4. Run "docker-compose up" to get Strapi running
5. Go to http://localhost:4200/ to see front end app
6. Go to http://localhost:1337/ to see the Strapi app
7. When you make changes to Strapi's data structure, new code will be written to the "strapi-app" folder
(which you can then review and commit as needed)


## Tips:
1. If you get an error while building docker: https://github.com/localstack/localstack/issues/480
2. If you are using the Docker Toolbox instead of the Docker Community Edition you will not be able to access the strapi admin interface via local host. You will need to use the following http://192.168.99.100:1337 instead of http://localhost:1337/ More info is available [here](https://forums.docker.com/t/cant-connect-to-container-on-localhost-with-port-mapping/52716/12)
3. It's a good idea to use lowercase letters only when naming data models in Strapi
